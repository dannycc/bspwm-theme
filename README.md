# BSPWM Theme

My Bspwm theme along with a polybar, rofi and picom setup
This is a minimalistic Linux setup for Debian 11 using BSPWM as the tiling window manager alongside rofi, polybar and picom.

[![YMWrrv.md.png](https://iili.io/YMWrrv.md.png)](https://freeimage.host/i/YMWrrv)

## Installation

### Download Debian
Make sure you install Debian using the iso from [here](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.2.0-amd64-netinst.iso) and install as you usually would, but do not install a desktop environment.

### Using the Script
Once you have successfully installed Debian and booted up into a shell, check for any updates then install git and clone my repository to your home folder.

**Updates**
```bash
sudo apt update && sudo apt upgrade -y
sudo apt install git -y
```

**Cloning the repository**
```bash
cd ~
git clone https://gitlab.com/dannycc/bspwm-theme.git
cd bspwm-theme
chmod +x run.sh dotfiles.sh
./run.sh
```

Installation will take quite a while as there are lots of dependencies required to build the programs, so wait patiently and ensure you say yes to any prompts.

## Usage

## Support
If you run into any issues, feel free to open an issue and I'll try my best to assist.

## Contributing

## Authors and acknowledgment
Huge inspiration and theming was taken from ChrisTitusTech's [Debian Titus](https://github.com/ChrisTitusTech/Debian-titus) as some of the main theming was used and any other external repository or resource.

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.

